import Util, {Link} from "./utils.js"
import DateTime from "./datetime.js"
import Modals from "./modal.js"

const User = (() => {
  class User {
    constructor(data) {
      this.data = data;
    }

    drawList() {
      let user = Util.createElementWithAttributes("div", "ad-user-list", null, {"aria-label": "user #" + this.data._adid});
      return user
    }

    drawModal() {
      let user = Util.createElementWithAttributes("div", "ad-user-list", null, {"aria-label": "user #" + this.data._adid});
      return user
    }

  }
  return User
})();

const UserSkeleton = (() => {
  const UserSkeleton = {
    drawList() {
      let user = Util.createElement("div", "ad-skel-user-list");
      return user
    }
  }

  return UserSkeleton
})();

export default User
export {UserSkeleton}