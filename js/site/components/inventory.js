import Util, {Link} from "./utils.js"
import DateTime from "./datetime.js"
import User, {UserSkeleton} from "./user.js"
import Modals from "./modal.js"

var _app = null;

const Inventory = (() => {

  const DefaultData = {
    name : "",
    type: "",
    state: "",
    description: "",
    extra: "",
    owner: "",
    location: "",
    labels: []
  }

  class Inventory {
    constructor(data) {
      this.data = {
        ...DefaultData,
        ...data
      }
    }

    drawList() {
      let item = Util.createElementWithAttributes("div", "ad-item-list", null, {"aria-label": "item #" + this.data._adid});
      let name = item.createChild("div", "ad-item-list-name");
      name.appendChild(Link.modal(this, this.data.name, item, Modals));
      name.createChild("span", "ad-item-list-type", ` (${this.data.type})`)
        let info = item.createChild("div", "ad-item-list-info");
        info.createChild("span", "ad-item-list-id", "#" + this.data._adid);
        if (this.data.owner != "") {
            info.createChild("span", "ad-item-list-text", "· owned by");
            let userSkel = UserSkeleton.drawInline();
            info.createChild("span", "ad-item-list-owner").appendChild(userSkel);
            _app.Util.loadResource(this.data.owner, User, userSkel, "drawInline");
        }
        if (this.data.state != "") {
            info.createChild("span", "ad-item-list-text", "· in");
            info.createChild("span", "ad-item-list-state", this.data.state);
            info.createChild("span", "ad-item-list-text", "condition");
        }
        if (this.data.location != "") {
            info.createChild("span", "ad-item-list-text", "· located in");
            info.createChild("span", "ad-item-list-location", this.data.location);
        }
      return item
    }

    drawNew() {
      return this.drawWindowEdit();
    }

  }
  return Inventory
})();

const InventorySkeleton = (() => {
  const InventorySkeleton = {
    drawList() {
      let item = Util.createElement("div", "ad-skel-item-list");
      item.createChild("div", "ad-skel-item-list-name");
      item.createChild("div", "ad-skel-item-list-info");
      return item
    },
    drawWindow() {
      let item = Util.createElement("div", "ad-skel-item-list");
      item.createChild("div", "ad-skel ad-skel-item-list-name");
      item.createChild("div", "ad-skel ad-skel-item-list-info");
      return item
    }
  }

  return InventorySkeleton
})();

export function initInventory(app) {
  _app = app;
}

export default Inventory
export {InventorySkeleton}