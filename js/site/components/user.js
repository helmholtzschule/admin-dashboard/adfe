import Util, {Link} from "./utils.js"
import Tooltip from "./tooltip.js"
import Badge, {BadgeSkeleton} from "./badge.js"

var _app = null;

const User = (() => {

  const AccessBadges = {
    0: {
      name: "access level 0",
      description: "get general server information",
      color: "#2c5364",
      content: "0",
      importance: 0,
    },
    1: {
      name: "access level 1",
      description: "get a entity by its id",
      color: "#2a4f5f",
      content: "1",
      importance: 0,
    },
    2: {
      name: "access level 2",
      description: "get stats of a category",
      color: "#274b5a",
      content: "2",
      importance: 0,
    },
    3: {
      name: "access level 3",
      description: "search for entities",
      color: "#224250",
      content: "3",
      importance: 0,
    },
    4: {
      name: "access level 4",
      description: "add a new entity",
      color: "#203e4b",
      content: "4",
      importance: 0,
    },
    5: {
      name: "access level 5",
      description: "update a entity",
      color: "#1e3a45",
      content: "5",
      importance: 0,
    },
    6: {
      name: "access level 6",
      description: "delete a entity",
      color: "#19313b",
      content: "6",
      importance: 0,
    },
    7: {
      name: "access level 7",
      description: "create a new user",
      color: "#162d36",
      content: "7",
      importance: 0,
    },
    8: {
      name: "access level 8",
      description: "full access",
      color: "#2c5364",
      content: "8",
      importance: 0,
    },
    9: {
      name: "access level 9",
      description: "full access and the ability to access entities he does not own",
      color: "#1e3a45",
      content: "9",
      importance: 0,
    }
  }

  class User {
    constructor(data) {
      this.data = data;
    }

    drawList() {
      let user = Util.createElementWithAttributes("div", "ad-user-list", null, {"aria-label": "user #" + this.data._adid});
      user.createChild("div", "ad-user-list-avatar ad-icon--user loading").createChildWithAttributes("img", "ad-user-list-avatar", null, {width: "80", src: this.data["icon-link"]}).addEvent("load", function() {this.parentNode.classList.remove("loading")});
      let info = user.createChild("div", "ad-user-list-info");
      let primary = info.createChild("div", "ad-user-list-info-primary");
      primary.createChild("span", "ad-user-name").appendChild(Link.window(this.data, this.data.name, "users"));;
      let badges = primary.createChild("span", "ad-user-badges");
      badges.appendChild((new Badge(AccessBadges[this.data["access-level"]]).drawInline()))
      if (this.data.badges) {
        this.data.badges.forEach(badgeRef => {
          let skel = BadgeSkeleton.drawInline();
          badges.appendChild(skel);
          Util.loadResource(badgeRef, Badge, skel, "drawInline");
        });
      }
      let secondary = info.createChild("div", "ad-user-list-info-secondary");
      secondary.createChild("span", "ad-user-username", "@" + this.data.username);
      secondary.createChild("span", "ad-user-id", "#" + this.data._adid);
      let emails = user.createChild("div", "ad-user-list-emails");
      if (this.data["public-emails"]) {
        this.data["public-emails"].slice(0,2).forEach(email => {
          emails.createChild("div", "ad-user-list-email", email);
        });
      }
      return user
    }

    drawModal() {
      let user = Util.createElementWithAttributes("div", "ad-user-list", null, {"aria-label": "user #" + this.data._adid});
      return user
    }

    drawInline() {
      let user = Util.createElementWithAttributes("span", "ad-user-inline", null, {"aria-label": "user #" + this.data._adid});

      let content = null;

      if (this.data._adid) {
        this.text = user.createChild("span", "ad-user-inline-text").appendChild(Link.window(this.data, this.data.name, "users"));
        content = user.createChild("div", "ad-user-inline-tooltip");
        content.createChild("div", "ad-user-inline-tooltip-avatar ad-icon--user loading").createChildWithAttributes("img", "ad-user-inline-tooltip-avatar", null, {width: "80", src: this.data["icon-link"]}).addEvent("load", function() {this.parentNode.classList.remove("loading")});
        let info = content.createChild("div");
        info.createChild("div", "ad-user-inline-tooltip-author", this.data.name);
        info.createChild("div", "ad-user-inline-tooltip-username", "@" + this.data.username + " · #" + this.data._adid);
      } else {
        this.text = user.createChild("span", "ad-user-inline-text", this.data.name);
        content = user.createChild("div", "ad-user-inline-tooltip", this.data.username);
      }

      this.tooltip = new Tooltip(user, content);
      return user
    }

  }
  return User
})();

const UserSkeleton = (() => {
  const UserSkeleton = {
    drawList() {
      let user = Util.createElement("div", "ad-skel-user-list");
      user.createChild("div", "ad-skel ad-skel-user-list-avatar");
      let info = user.createChild("div", "ad-skel-user-list-info");
      info.createChild("div", "ad-skel ad-skel-user-list-name");
      info.createChild("div", "ad-skel ad-skel-user-list-username");
      return user
    },
    drawInline() {
      let user = Util.createElement("span", "ad-skel ad-skel-user-inline");
      return user
    }
  }

  return UserSkeleton
})();

export function initUser(app) {
  _app = app;
}
export default User
export {UserSkeleton}