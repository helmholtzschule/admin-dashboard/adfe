import Util, {Link} from "./utils.js"
import Tooltip from "./tooltip.js"

const Badge = (() => {
  class Badge {
    constructor(data) {
      this.data = data;
    }

    drawList() {
      let badge = Util.createElementWithAttributes("div", "ad-badge-list", null, {"aria-label": "badge"});
      return badge
    }

    drawModal() {
      let badge = Util.createElementWithAttributes("div", "ad-badge-list", null, {"aria-label": "badge"});
      return badge
    }

    drawInline() {
      let badge = Util.createElementWithAttributes("span", "ad-badge-inline", null, {"aria-label": "badge"});
      
      badge.createChildWithAttributes("span", "ad-badge-inline-text", null, {"data-content": this.data.content, style: "background: " + this.data.color});
      let content = badge.createChild("div", "ad-badge-inline-tooltip");
      content.createChild("div").createChildWithAttributes("span", "ad-badge-inline-text", null, {"data-content": this.data.content, style: "background: " + this.data.color});
      let info = content.createChild("div");
      info.createChild("div", "ad-badge-name", this.data.name);
      info.createChild("div", "ad-badge-description", this.data.description);
      new Tooltip(badge, content);
      return badge
    }
  }
  return Badge
})();

const BadgeSkeleton = (() => {
  const BadgeSkeleton = {
    drawList() {
      let badge = Util.createElement("div", "ad-skel ad-skel-badge-list");
      return badge
    },
    drawInline() {
      let badge = Util.createElement("span", "ad-skel ad-skel-badge-inline");
      return badge
    }
  }

  return BadgeSkeleton
})();

export default Badge
export {BadgeSkeleton}