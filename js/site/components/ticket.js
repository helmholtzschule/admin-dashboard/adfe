import Util, {Link} from "./utils.js"
import DateTime from "./datetime.js"
import Modals from "./modal.js"
import User, {UserSkeleton} from "./user.js"

var _app = null;

const Ticket = (() => {
  class Ticket {
    constructor(data) {
      this.data = data;
    }

    drawList() {
      let ticket = Util.createElementWithAttributes("div", "ad-ticket-list ad-ticket-" + ((this.data.ticket.archived) ? "closed" : "open"), null, {"aria-label": "ticket #" + this.data._adid});
        let primary = ticket.createChild("div", "ad-ticket-list-primary");
        primary.createChild("div", "ad-ticket-list-title").appendChild(Link.modal(this, this.data.ticket.title, Ticket, Modals));
        let info = primary.createChild("div", "ad-ticket-list-info");
        info.createChild("span", "ad-ticket-list-id", "#" + this.data._adid);
        info.createChild("span", "ad-ticket-list-text", "· opened by");
        let user = new User({_adid: null, name: this.data.name, username: this.data.username});
        info.createChild("span", "ad-ticket-list-author").appendChild(user.drawInline());
        let datetime = new DateTime(info.createChild("span", "ad-ticket-list-date"), this.data.time, {interval: 10000});
        let icon = info.createChild("span", "ad-ticket-list-severity")
        icon.createChild("span", "ad-icon--severity ad-ticket-list-severity-icon");
        icon.createChild("span", "ad-ticket-list-severity-text", this.data.ticket.severity)
        ticket.createChild("div", "ad-ticket-list-secondary").createChildWithAttributes("a", "btn btn-outline-success ad-ticket-list-convert", "convert to issue", {href: _app.adUrl("issues/new", {ticket: {id: this.data._adid}})}).addEvent("click", (e) => {
          this.data.ticket.archived = true;
          Util.RequestAsync("PUT", _app.adapiUrl("/api/tickets"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
        });
      return ticket
    }

    drawModal() {
      let ticket = Util.createElementWithAttributes("div", "ad-ticket-modal ad-ticket-" + ((this.data.ticket.archived) ? "closed" : "open"), null, {"aria-label": "ticket #" + this.data._adid});
      let content = ticket.createChild("div", "ad-ticket-modal-content");
      let sidebar = ticket.createChild("aside", "ad-ticket-modal-sidebar").createChild("div", "ad-ticket-modal-wrapper");
      let contentHeader = content.createChild("div", "ad-ticket-modal-content-header");
      contentHeader.createChild("span", "ad-ticket-modal-id", "#" + this.data._adid);
      contentHeader.createChild("span", "ad-ticket-modal-state", (this.data.ticket.archived) ? "closed" : "open");
      contentHeader.createChild("span", "ad-ticket-modal-text", "opened");
      new DateTime(contentHeader.createChild("span", "ad-ticket-modal-date"), this.data.time, {interval: 10000});
      contentHeader.createChild("span", "ad-ticket-modal-text", "by");
      contentHeader.createChild("span", "ad-ticket-modal-author", this.data.name);
      let contentBody = content.createChild("div", "ad-ticket-modal-content-body");
      contentBody.createChild("div", "ad-ticket-modal-title", this.data.ticket.title);
      contentBody.createChild("div", "ad-ticket-modal-description", this.data.ticket.description);

      let sidebarHeader = sidebar.createChild("div", "ad-ticket-modal-sidebar-header");
      if (this.data.ticket.archived) {
        sidebarHeader.createChild("button", "btn btn-outline-warning ad-ticket-modal-reopen", "reopen Ticket").addEvent("click", (e) => {
          this.data.ticket.archived = false;
          Util.RequestAsync("PUT", _app.adapiUrl("/api/tickets"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
        });
      } else {
        sidebarHeader.createChild("button", "btn btn-outline-warning ad-ticket-modal-close", "close Ticket").addEvent("click", (e) => {
          this.data.ticket.archived = true;
          Util.RequestAsync("PUT", _app.adapiUrl("/api/tickets"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
        });
      }
      sidebarHeader.createChildWithAttributes("a", "btn btn-outline-success ad-ticket-modal-convert", "convert to issue", {href: _app.adUrl("issues/new", {ticket: {id: this.data._adid}})}).addEvent("click", (e) => {
        this.data.ticket.archived = true;
        Util.RequestAsync("PUT", _app.adapiUrl("/api/tickets"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
      });
      let sidebarBody = sidebar.createChild("div", "ad-ticket-modal-sidebar-body");

      function addItemToSidebar(key, value) {
        let item = sidebarBody.createChild("div", "ad-ticket-modal-sidebar-item");
        item.createChild("div", "ad-ticket-item-key", key);
        item.createChild("div", "ad-ticket-item-value" + ((value) ? "" : " ad-empty"), (value) ? value : "none");
      }

      addItemToSidebar("severity", this.data.ticket.severity);
      addItemToSidebar("room", this.data.ticket.room);
      addItemToSidebar("involved items", this.data.ticket.involved_items);

      return ticket
    }
  }
  return Ticket
})();

const TicketSkeleton = (() => {
  const TicketSkeleton = {
    drawList() {
      let ticket = Util.createElement("div", "ad-skel-ticket-list");
      ticket.createChild("div", "ad-skel ad-skel-ticket-list-title");
      ticket.createChild("div", "ad-skel ad-skel-ticket-list-info");
      return ticket
    },
    drawWindow() {
      let ticket = Util.createElement("div", "ad-skel-ticket-list");
      ticket.createChild("div", "ad-skel ad-skel-ticket-list-title");
      ticket.createChild("div", "ad-skel ad-skel-ticket-list-info");
      return ticket
    }
  }

  return TicketSkeleton
})();

export function initTicket(app) {
  _app = app;
}

export default Ticket
export {TicketSkeleton}