import Util, {Link} from "./utils.js"
import DateTime from "./datetime.js"
import User, {UserSkeleton} from "./user.js"
import Modals from "./modal.js"

var _app = null;

const Issue = (() => {

  const DefaultData = {
    title : "",
    description : "",
    open: true,
    working: false,
    "related-issues" : [],
    labels : [],
    tag: "",
    milestone: "",
    time : {
      spent: [],
      due: -1,
      opened: -1,
      closed: -1,
      assigned: -1,
      working: -1,
      reviewed: -1
    },
    weight: 0,
    assignees : [],
    activity : []
  }

  class Issue {
    constructor(data) {
      this.data = {
        ...DefaultData,
        ...data
      }
    }

    drawList() {
      let issue = Util.createElementWithAttributes("div", "ad-issue-list ad-issue-" + ((!this.data.open) ? "closed" : "open"), null, {"aria-label": "issue #" + this.data._adid});
      issue.createChild("div", "ad-issue-list-title").appendChild(Link.modal(this, this.data.title, Issue, Modals));
        let info = issue.createChild("div", "ad-issue-list-info");
        info.createChild("span", "ad-issue-list-id", "#" + this.data._adid);
        info.createChild("span", "ad-issue-list-text", "· opened by");
        let userSkel = UserSkeleton.drawInline();
        info.createChild("span", "ad-ticket-list-author").appendChild(userSkel);
        _app.Util.loadResource("US" + this.data._owner, User, userSkel, "drawInline");
        let datetime = new DateTime(info.createChild("span", "ad-issue-list-date"), this.data.time.opened, {interval: 10000});
        let icon = info.createChild("span", "ad-issue-list-severity")
        icon.createChild("span", "ad-icon--severity ad-issue-list-severity-icon");
        icon.createChild("span", "ad-issue-list-severity-text", this.data.weight)
      return issue
    }

    drawModal() {
      let issue = Util.createElementWithAttributes("div", "ad-issue-modal ad-issue-" + ((!this.data.open) ? "closed" : "open"), null, {"aria-label": "issue #" + this.data._adid});
      let content = issue.createChild("div", "ad-issue-modal-content");
      let sidebar = issue.createChild("aside", "ad-issue-modal-sidebar").createChild("div", "ad-issue-modal-wrapper");
      let contentHeader = content.createChild("div", "ad-issue-modal-content-header");
      contentHeader.createChild("span", "ad-issue-modal-id", "#" + this.data._adid);
      contentHeader.createChild("span", "ad-issue-modal-state", (!this.data.open) ? "closed" : "open");
      contentHeader.createChild("span", "ad-issue-modal-text", "opened");
      new DateTime(contentHeader.createChild("span", "ad-issue-modal-date"), this.data.time.opened, {interval: 10000});
      contentHeader.createChild("span", "ad-issue-modal-text", "by");
      contentHeader.createChild("span", "ad-issue-modal-author", this.data.name);
      let contentBody = content.createChild("div", "ad-issue-modal-content-body");
      contentBody.createChild("div", "ad-issue-modal-title", this.data.title);
      contentBody.createChild("div", "ad-issue-modal-description", this.data.description);

      let sidebarHeader = sidebar.createChild("div", "ad-issue-modal-sidebar-header");
      contentHeader.createChild("button", "btn btn-outline-secondary ad-issue-modal-edit", "edit").addEvent("click", (e) => {
        issue.parentNode.replaceChild(this.drawWindowEdit(), issue);
      });
      sidebarHeader.createChild("button", "btn btn-outline-warning ad-issue-modal-close", "close issue").addEvent("click", (e) => {
        this.data.open = false;
        Util.RequestAsync("PUT", _app.adapiUrl("/api/issues"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
      });
      let sidebarBody = sidebar.createChild("div", "ad-issue-modal-sidebar-body");

      return issue
    }



    drawWindow() {
      let issue = Util.createElementWithAttributes("div", "ad-issue-window ad-issue-" + ((!this.data.open) ? "closed" : "open"), null, {"aria-label": "issue #" + this.data._adid});
      let content = issue.createChild("div", "ad-issue-window-content");
      let contentHeader = content.createChild("div", "ad-issue-window-content-header");
      contentHeader.createChild("span", "ad-issue-window-id", "#" + this.data._adid);
      contentHeader.createChild("span", "ad-issue-window-state", (!this.data.open) ? "closed" : "open");
      contentHeader.createChild("span", "ad-issue-window-text", "opened");
      new DateTime(contentHeader.createChild("span", "ad-issue-window-date"), this.data.time.opened, {interval: 10000});
      contentHeader.createChild("span", "ad-issue-window-text", "by");
      contentHeader.createChild("span", "ad-issue-window-author", this.data.name);

      contentHeader.createChild("span", "ad-issue-window-spacer");

      contentHeader.createChild("button", "btn btn-outline-secondary ad-issue-window-edit", "edit").addEvent("click", (e) => {
        issue.parentNode.replaceChild(this.drawWindowEdit(), issue);
      });
      contentHeader.createChild("button", "btn btn-outline-warning ad-issue-window-close", "close issue").addEvent("click", (e) => {
        this.data.open = false;
        Util.RequestAsync("PUT", _app.adapiUrl("/api/issues"), () => console.log(this), {data: [this.data], find: {_adid: this.data._adid}});
      });

      let contentBody = content.createChild("div", "ad-issue-window-content-body");
      contentBody.createChild("div", "ad-issue-window-title", this.data.title);
      contentBody.createChild("div", "ad-issue-window-description", this.data.description);
      let details = contentBody.createChild("div", "ad-issue-window-details");

      return issue
    }

    drawWindowEdit() {
      let editData = this.data;

      let issue = Util.createElementWithAttributes("div", "ad-issue-window-edit ad-issue-" + ((!this.data.open) ? "closed" : "open"), null, {"aria-label": "issue #" + this.data._adid});
      let content = issue.createChild("div", "ad-issue-window-content");
      let contentHeader = content.createChild("div", "ad-issue-window-content-header");
      contentHeader.createChild("span", "ad-issue-window-id", "#" + this.data._adid);
      contentHeader.createChild("span", "ad-issue-window-state", (!this.data.open) ? "closed" : "open");
      contentHeader.createChild("span", "ad-issue-window-text", "opened");
      new DateTime(contentHeader.createChild("span", "ad-issue-window-date"), this.data.time.opened, {interval: 10000});
      contentHeader.createChild("span", "ad-issue-window-text", "by");
      contentHeader.createChild("span", "ad-issue-window-author", this.data.name);

      contentHeader.createChild("span", "ad-issue-window-spacer");

      contentHeader.createChild("button", "btn btn-outline-warning ad-issue-window-close", "close issue");
      let contentBody = content.createChild("div", "ad-issue-window-content-body");
      contentBody.createChildWithAttributes("input", "ad-issue-window-title", null, {value: this.data.title}).bind(editData, ["title"]);
      contentBody.createChild("textarea", "ad-issue-window-description", this.data.description).bind(editData, ["description"]);
      let details = contentBody.createChild("div", "ad-issue-window-details");

      let contentFooter = content.createChild("div", "ad-issue-window-content-footer");
      contentFooter.createChild("button", "btn btn-outline-success ad-issue-window-save", "save").addEvent("click", (e) => {
        this.data = editData;
        console.log({data: [this.data]});
        Util.RequestAsync("POST", _app.adapiUrl("/api/issues/create"), () => console.log(this), {data: [this.data]});
        issue.parentNode.replaceChild(this.drawWindow(), issue);
      });
      contentFooter.createChild("button", "btn btn-outline-primary ad-issue-window-cancel", "cancel").addEvent("click", (e) => {
        window.history.back();
      });

      return issue
    }

    drawNew() {
      return this.drawWindowEdit();
    }

  }
  return Issue
})();

const IssueSkeleton = (() => {
  const IssueSkeleton = {
    drawList() {
      let issue = Util.createElement("div", "ad-skel-issue-list");
      issue.createChild("div", "ad-skel-issue-list-title");
      issue.createChild("div", "ad-skel-issue-list-info");
      return issue
    },
    drawWindow() {
      let issue = Util.createElement("div", "ad-skel-issue-list");
      issue.createChild("div", "ad-skel ad-skel-issue-list-title");
      issue.createChild("div", "ad-skel ad-skel-issue-list-info");
      return issue
    }
  }

  return IssueSkeleton
})();

export function initIssue(app) {
  _app = app;
}

export default Issue
export {IssueSkeleton}