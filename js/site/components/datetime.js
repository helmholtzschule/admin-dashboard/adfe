import Util from "./utils.js"
import Tooltip from "./tooltip.js"

const DateTime = (() => {

  const Default = {
    update: true,
    interval: 4000
  }

  class DateTime {
    constructor(inline, time, config) {
      this.config = {
        ...Default,
        ...config
      };

      this.time = moment.unix(time);
      this.text = inline.createChild("span", "ad-datetime-text", this.time.fromNow());
      let content = inline.createChild("span", "ad-datetime-tooltip", this.time.format("DD.MM.YYYY HH:mm:ss"));

      this.interval = setInterval(() => {
        this.update()
      }, this.config.interval);

      this.tooltip = new Tooltip(inline, content);
    }

    update() {
      this.text.innerHTML = this.time.fromNow();
    }
  }
  return DateTime
})();

export default DateTime