import Util, {Link, initUtil} from "../components/utils.js"
import Notification from "../components/notifications.js"
import User, {UserSkeleton, initUser} from "../components/user.js"
import Modals from "../components/modal.js"

const App = (() => {
  var Default = {
    draw: {
      removeBodyContent: false,
      navTop: true,
      navSide: true,
      contentWrapper: true,
      notificationFloating: true,
      modalFloating: true
    },
    adapi: {
      host: "10.8.2.74",
      port: 8080,
      protocol: "http"
    },
    app: {
      view: "list"
    },
    pages: {
      issues: {
        icon: "issues",
        name: "Issues",
        order: 5,
        url: "issues/issues",
        content: {
          tickets: {
            name: "Tickets",
            order: 20,
            url: "issues/tickets",
            count: 20
          },
          list: {
            name: "List",
            order: 10,
            url: "issues/issues"
          },
          new: {
            name: "New",
            order: 20,
            url: "issues/new"
          }
        }
      },
      users: {
        icon: "users",
        name: "Users",
        order: 5,
        url: "users/users"
      },
      inventory: {
        icon: "inventory",
        name: "Inventory",
        order: 2,
        url: "inventory/inventory"
      }
    },
    root: "adfe/pages"
  }
  class App {
      constructor(config, elementsInitHandler) {
        this.config = {
          ...Default,
          ...config
        };
        this.Util = Util;
        this.notification = new Notification();
        this.Entities = {};
        this.elements = {};
        this.filter = {};
        this.userControls = {};
        this.page = "dashboard";
        this.path = ["dashboard"];
        initUtil(this);
        initUser(this);
        this.loadUrl();
        this.elementsInitHandler = elementsInitHandler;
        this.user = document.cookie.split("; ").reduce((a,b)=> {let spl = b.split("="); return (a[spl[0]] = Util.toNumIfPossible(spl[1]) , a)}, {});
        this.initElements();

      }
      initElements() {
        let body = document.getElementsByTagName("body")[0];
        this.elements.body = body;
        let wrapper = this.Util.createElement("div", "page-layout");
        if (this.config.draw.removeBodyContent) {body.removeChildren()}
        if (this.config.draw.contentWrapper) {
          this.elements.contentWrapper = this.Util.createElement("div", "content-wrapper");
          if (!this.config.draw.removeBodyContent) {
            while (body.childNodes.length > 0) {
              if (!["#text", "SCRIPT"].includes(body.childNodes[0].nodeName)) {this.elements.contentWrapper.appendChild(body.childNodes[0])} else {body.removeChild(body.childNodes[0])}
            }
          }
          wrapper.appendChild(this.elements.contentWrapper);
        }
        if (this.config.draw.navTop) {this.elements.navTop = body.createChildWithAttributes("header", "nav-top", null, {id: "nav-top"}); this.drawNavTop()}
        body.appendChild(wrapper);
        if (this.config.draw.navSide) {this.elements.navSide = wrapper.createChildWithAttributes("nav", "nav-side", null, {id: "nav-left"})}
        this.elements.mobileOverlay = wrapper.createChild("div", "mobile-overlay");
        if (this.config.draw.notificationFloating) {this.elements.notificationFloating = wrapper.createChildWithAttributes("div", "notification-panel", null, {id: "notification-panel-floating"}); this.notification.setPlaceContainer("floating", this.elements.notificationFloating)}
        if (this.config.draw.modalFloating) {this.elements.modalFloating = wrapper.createChildWithAttributes("div", "modal-panel", null, {id: "modal-panel-floating"}); Modals.setContainer(this.elements.modalFloating)}
        if (this.elementsInitHandler != null) {this.elementsInitHandler(this)}

        if (this.config.draw.navSide) {this.drawNavLeft()}
      }
      drawNavTop() {
        let nav = this.elements.navTop;
        nav.createChild("span", "ad-nav-top-spacer");
        nav.createChild("button", "ad-logout btn btn-light", "sign out").addEvent("click", () => {
          Util.RequestAsync("GET", this.adapiUrl("/logout"), () => {window.open(window.location.origin + "/adfe/pages/logout.html", "_self")})
        }).createChild("span", "ad-icon--logout");
      }
      drawNavLeft() {
        function sortItems(items) {
          let x = Object.entries(items).sort((a, b) => {
            let order = a[1].order - b[1].order;
            return (order != 0)? order : a[0].localeCompare(b[0]);
          })
          x.forEach(item => {
            if (item[1].content) {
              item[1].content = sortItems(item[1].content);
            }
          });
          return x
        }
        let navItems = sortItems(this.config.pages);
        let nav = this.Util.createElement("div", "nav-left-scroll");
        nav.createChild("div", "nav-header").createChildWithAttributes("a", "", null, {href: this.adUrl("dashboard")}).createChild("span", "nav-item-name", "Dashboard");
        this.elements.navSide.appendChild(nav);
        let drawItems = (items, level) => {
          let list = this.Util.createElement("ul", "nav-level-" + level);
          items.forEach(item => {
            let li = list.createChild("li", (item[1].url.split("/")[level] == this.path[level])? "active" : "");
            let a = li.createChildWithAttributes("a", "", null, {href: this.adUrl(item[1].url), title: item[1].name});
            if (item[1].icon) {
              a.createChild("div", "nav-item-icon ad-icon--" + item[1].icon);
            }
            a.createChild("span", "nav-item-name", item[1].name);
            if (item[1].count || item[1].count == 0) {
              a.createChild("span", "nav-item-badge ad-pill", item[1].count);
            }
            if (item[1].content) {
              li.appendChild(drawItems(item[1].content, level + 1));
            }
          });
          return list;
        }
        nav.appendChild(drawItems(navItems, 0));
        this.elements.mobileOverlay.addEvent("click", () => {this.toggleNavLeft()});
        this.calcNavLeftSize();
      }
      calcNavLeftSize() {
        this.elements.navSide.classList.toggle("nav-desktop", true);
      }
      toggleNavLeft() {
        this.elements.navSide.classList.toggle("nav-expanded");
      }
      addUserControl(control) {
        this.userControls[control.config.name] = control;
        control.elements.root.addEventListener("ad-control-change", (e) => {this.setFilterForForm(e.detail.vals, e.detail.obj.config.name); this.updateUrl()});
      }
      loadUrl() {
        let pathname = window.location.pathname;
        this.page = pathname.substring(pathname.lastIndexOf('/')+1, pathname.indexOf('.'));
        this.path = pathname.substring(pathname.lastIndexOf(this.config.root) + this.config.root.length + 1, pathname.lastIndexOf('.')).split("/");
        let params = (new URL(window.location.href)).searchParams.entries();
        let result = params.next();
        while (!result.done) {
          let split = result.value[0].split(".", 2);
          if (split[0] == "app") {
            this.config.app[split[1]] = Util.toNumIfPossible(result.value[1]);
          } else {
            if (this.filter[split[0]]) {
              this.filter[split[0]][split[1]] = Util.toNumIfPossible(result.value[1]);
            } else {
              this.filter[split[0]] = {[split[1]]: Util.toNumIfPossible(result.value[1])};
            }
          }
          result = params.next();
        }
        document.title = this.getPathName(this.path, this.config.pages).join(" / ") + " | Admin Dashboard";
        
      }
      getPathName(path, pathObj){
        let getPName = (path, pathObj) => {
          if (path.length < 1 || !pathObj.hasOwnProperty(path[0])) {
            return []
          } else {
            let elems = [pathObj[path[0]].name];
            elems.push(...((pathObj[path[0]].hasOwnProperty("content"))? getPName(path.slice(1), pathObj[path[0]].content) : []));
            return elems
          }
        };
        let res = getPName(path, pathObj);
        return res
      } 
      updateUrl() {
        let appString = Object.entries(this.config.app).map(([key, value]) => {
          if (value != undefined) {
            return `${encodeURIComponent("app." + key)}=${encodeURIComponent(value)}`;
          }
        }).join('&');
        let urlString = Object.entries(this.filter).map(([formName, form]) => {
          return Object.entries(form).filter(([key, value]) => {
            if (value == undefined || value == "") {
              return false
            }
            return true
          }).map(([key, value]) => {
            return `${encodeURIComponent(formName + "." + key)}=${encodeURIComponent(value)}`;
          }).join('&');
        })/* .filter(str => {
          if (str == "") {
            return true
          }
          return true
        }) */.join('&');
        history.replaceState({}, undefined, this.page + ".html?" + appString + ((urlString == "") ? "" : "&" + urlString));
      }
      setFilterForForm(filter, form) {
        this.filter[form] = filter;
        this.updateUrl();
      }
      loadControlValues(control) {
        if (!control) {
          for (let control in this.userControls) {
            if (this.userControls.hasOwnProperty(control)) {
              this.userControls[control].setFormVals(this.filter[control]);
            }
          }
        } else {
          this.userControls[control].setFormVals(this.filter[control]);
        }
      }
      adapiUrl(path) {
        return this.config.adapi.protocol + "://"+ this.config.adapi.host + ":" + this.config.adapi.port + path
      }
      adUrl(page, parameters) {
        let urlString = "";
        if (parameters) {
          urlString = Object.entries(parameters).map(([controlName, control]) => {
            return Object.entries(control).filter(([key, value]) => {
              if (value == undefined || value == "") {
                return false
              }
              return true
            }).map(([key, value]) => {
              return `${encodeURIComponent(controlName + "." + key)}=${encodeURIComponent(value)}`;
            }).join('&');
          }).join('&');
        }
        return "/" + this.config.root + "/" + page + ".html?" + urlString
      }
      getConfig() {
        return config
      }
    }
  return App
})()
export default App