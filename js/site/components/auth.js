function authenticate() {
  if (!document.cookie.includes("authenticated=true")) {
    if (!window.location.pathname.endsWith("login.html")) {
      window.open("/adfe/pages/login.html?redirect=" + encodeURIComponent(window.location.pathname + window.location.search), "_self");
    }
  } else {
    if (window.location.pathname.endsWith("login.html")) {
      redirect()
    }
    return true
  }
  return false
}

function redirect() { 
  let url = decodeURIComponent((new URL(window.location.href)).searchParams.get("redirect"))
  window.open((url != "null")? window.location.origin + url : window.location.origin + "/adfe/pages/index.html", "_self");
}

export default authenticate
export {redirect}