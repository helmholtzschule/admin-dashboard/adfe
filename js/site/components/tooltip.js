import Util from "./utils.js"

const Tooltip = (() => {
  class Tooltip {
    constructor(ref, content) {
      let tooltip = Util.createElement("div", "ad-tooltip");
      tooltip.append(content);
      ref.append(tooltip);
      ref.addEvent("mouseover", () => {this.show()}).addEvent("mouseout", () => {this.hide()});
      this.tooltipElement = tooltip;
      this.popper = new Popper(ref, tooltip);
    }
    
    show() {
      this.tooltipElement.classList.toggle("ad-tooltip-show", true);
    }

    hide() {
      this.tooltipElement.classList.toggle("ad-tooltip-show", false);
    }
  }
  return Tooltip
})();

export default Tooltip