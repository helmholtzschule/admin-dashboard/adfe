import Util from "./utils.js"

const List = (() => {
  // basic clas names to be used
  const ClassNames = {
    root:         "ad-list",
    header:       "ad-list-header",
    body:         "ad-list-body",
    skeletonBody: "ad-list-skeleton-body",
    footer:       "ad-list-footer"
  };

  // default configuration
  const Default = {
    // limit the amount of entities that are displayed at once.
    // if false the loadHandler gets called wen the user scolls down enough // TODO
    limitEntities:    true,
    // the amount of entities displayed on one page
    entitiesPerPage:  5,
    // the total amount of entities
    entitiesCount:    5,
    // maximum number of pages to be displayed in the pagination
    navMaxPageCount:  7,
    // the numer of the page currently displayed
    pageNumber:       0,
    // show a search field in the top right corner
    searchField:      true,
    // draw the list once the object is initialized
    showOnConstruct:  true,
    // load entities into the list once the object is initialized
    loadOnConstruct:  false,
    // name of the object in case there are multiple
    name:             "list",
    // category of items (displayed in the search field for example)
    category:         "entities",

    // list of inline controls
    // HTMLObject or
    // object in the form: {name: "name", type:(optional) "type", placeholder:(optional) "placeholder"}
    inlineControls:   [],
    // object in the form: {name: "name", type:(optional) "type", placeholder:(optional) "placeholder"}
    advancedControls: [],
    // time before a change in 'live' inputs is applied
    timeout:          500,
    // a function to create a skeleton placeholder
    skeletonFunction: null,
  };

  class List {
    constructor(element, config, handler) {
      // apply config and default config
      this.config = {
        ...Default,
        ...config
      };
      // setup elements object
      this.elements = {
        container: element,
        root: null,
        header: null,
        body: null,
        footer: null,
        advanced: {},
        paginations: {},
        forms: [],
        inputs: {}
      }
      // set the list no not drawn
      this.drawn = false;
      // set handler responsible for loading entities
      this.handler = (handler) ? handler : function() {console.log("no valid handler for loading entities set")};
      // if set in the config draw directly after initilization
      if (this.config.showOnConstruct) {
        this.draw();
        if (this.config.loadOnConstruct) {
          this.load();
        }
      }
    }

    set entitiesCount(v) {
      this.config.entitiesCount = v;
      this.ChangePageTo(this.config.pageNumber);
    }

    set entitiesPerPage(v) {
      this.config.entitiesPerPage = v;
      this.ChangePageTo(this.config.pageNumber);
    }

    set pageNumber(v) {
      this.ChangePageTo(v);
    }

    draw() {
      // remove all elements inside the container
      this.elements.container.removeChildren();
      // create root div and save in temporary variable (convenience)
      let root = Util.createElement("div", ClassNames.root);
      root.control = this;
      // save root in elements
      this.elements.root = root;
      // create header, body and footer and save in elements
      this.elements.header = root.createChild("div", ClassNames.header);
      this.elements.body = root.createChild("div", ClassNames.body);
      this.elements.skeletonBody = root.createChild("div", ClassNames.skeletonBody);
      this.elements.footer = root.createChild("div", ClassNames.footer);

      //// header

      // if there are any advanced controls specified
      if (this.config.advancedControls.length > 0) {
        // create button to open menu and add event handler. aslo add the button to the header
        let button = Util.createElement("button", "btn btn-outline-secondary ad-list-button-advanced ad-icon--advanced").addEvent("click", (e) => {e.preventDefault(); this._toggleAdvanced()});
        this._addControlTo(button, this.elements.header);
        // create container for advanced controls
        this.elements.advanced.container = this.elements.header.createChild("div", "ad-list-advanced");
        // create a form inside the container
        let form = this.elements.advanced.container.createChild("form", "ad-list-advanced-controls");
        this.elements.advanced.form = form;
        // add to forms array for evaluation
        this.elements.forms.push(form);
        // create a body to which all advanced control elements will be added
        this.elements.advanced.body = form.createChild("div", "ad-list-advanced-body");
        // create a footer
        let footer = form.createChild("div", "ad-list-advanced-footer");
        // create a button to close the menu without search
        footer.createChild("button", "btn btn-outline-secondary ad-list-advanced-close", "close").addEvent("click", (e) => {e.preventDefault(); this._toggleAdvanced()});
        // create a button to close the menu with search
        footer.createChild("button", "btn btn-outline-success ad-list-advanced-search", "search").addEvent("click", (e) => {e.preventDefault(); this._toggleAdvanced(); this._handleSearch(e)});
        // for every control passed
        this.config.advancedControls.forEach(control => {
          if (control instanceof HTMLElement) {
            // if it is a html element and has not an empty name add the control to the advanced menu
            if (control.name != "") {
              this._addControlToAdvanced(control);
            }
          } else if (control.name != "" && typeof control.name == "string") {
            // if the object has just a name of type string
            // create a new input and set attributes
            let elem = Util.createElement("input", "form-control");
            elem.name = control.name;
            this.elements.inputs[elem.name] = elem;
            // if not specified use text type
            elem.type = (control.type != "" && typeof control.type == "string") ? control.type : "text";
            elem.placeholder = (control.placeholder != "" && typeof control.placeholder == "string") ? control.placeholder : "";
            // add the control to the advanced menu
            this._addControlToAdvanced(elem);
          }
        });
      }

      // if there were any inline controls specified
      if (this.config.inlineControls.length > 0) {
        // for every control passed
        this.config.inlineControls.forEach(control => {
          if (control instanceof HTMLElement) {
            // if it is a html element and has not an empty name add the control to the header
            if (control.name != "") {
              this._addControlTo(control, this.elements.header);
            }
          } else if (control.name != "" && typeof control.name == "string") {
            // if the object has just a name of type string
            // create a new input and set attributes
            let elem = Util.createElement("input", "form-control");
            elem.name = control.name;
            elem.id = control.name;
            this.elements.inputs[elem.name] = elem;
            // if not specified use text type
            elem.type = (control.type != "" && typeof control.type == "string") ? control.type : "text";
            elem.placeholder = (control.placeholder != "" && typeof control.placeholder == "string") ? control.placeholder : "";
            // if the control should update the list 'live'
            if (control.live == true) {
              elem.addEvent("keypress", (e) => this._handleInputChange(e));
            }
            // if the control should update the list 'onchange'
            if (control.onchange == true) {
              elem.addEvent("change", (e) => this._handleInputChange(e));
            }
            // add the control to the header
            if (control.label != "") {
              let label = Util.createElementWithAttributes("label", "form-label", control.label, {for: control.name});
              let group = Util.createElement("span", "form-group");
              group.appendChild(elem);
              group.appendChild(label);
              this._addControlTo(group, this.elements.header);
            } else {
              this._addControlTo(elem, this.elements.header);
            }
          }
        });
      }

      // add a spacer
      this._addSpacerTo(this.elements.header);

      // add a pagination control with the id 1
      this._addPaginationTo(1, this.elements.header);

      // add a spacer
      this._addSpacerTo(this.elements.header);

      // if specified in the config add a search field
      if (this.config.searchField) {
        // create a new input group
        let group = Util.createElement("div", "input-group " + ClassNames.root + "-search");
        // add a input with attributes
        this.elements.inputs["search"] = group.createChildWithAttributes("input", "form-control", null, {name: "search", placeholder: `search in ${this.config.category}`}).addEvent("keypress", (e) => this._handleInputChange(e));
        // add a button inside a div as a apended button to the group
        group.createChild("div", "input-group-append").createChild("button", "btn btn-outline-secondary", "go").addEvent("click", (e) => {e.preventDefault(); this._handleSearch(e)});
        // add the control to the header
        this._addControlTo(group, this.elements.header);
      }

      this._drawSkeleton();  

      //// footer

      // add a spacer
      this._addSpacerTo(this.elements.footer);

      // add a pagination control with the id 2
      this._addPaginationTo(2, this.elements.footer);

      // add a spacer
      this._addSpacerTo(this.elements.footer);


      // add the root element to the container
      this.elements.container.appendChild(root);
      // set the list to drawn
      this.drawn = true;
    }

    // add element to the container but wrap it inside a span and form element
    _addControlTo(control, container) {
      let form = container.createChild("span", container.classList[0] + "-control").createChild("form", "ad-list-control")
      // add control to form
      form.appendChild(control);
      // add the form to forms list for evaluation
      this.elements.forms.push(form);
    }

    // add a span element as space
    _addSpacerTo(container) {
      container.createChild("span", container.classList[0] + "-spacer");
    }

    // add element to the container but wrap it inside a span and form element
    _addControlToAdvanced(control) {
      this.elements.advanced.body.createChild("span", "ad-list-advanced-control").appendChild(control);
    }

    // add a pagination control to the container
    _addPaginationTo(id, container) {
      // if a pagination control of this id already exists use the old root div
      let root;
      if (this.elements.paginations[id]) {
        root = this.elements.paginations[id].root;
      } else {
        // if not create a new root div
        root = container.createChildWithAttributes("nav", container.classList[0] + "-pagination", null, {"aria-label": this.config.category + "pages"});
      }
      // create a new guttn group inside the nav element
      let btnGroup = root.createChild("div", "btn-group");
      // calculate the page numbers
      let pages = this._calcPageNumbers();
      // save element references in the elements object
      this.elements.paginations[id] = {btnCount: pages.count, btns: {}, container: container, group: btnGroup, root: root};
      // to all clickable buttons the 'data-page' attribute is set to the page index which should get visited when clicked.
      // Also a EventHadler is added to handle the click event
      // add the previous button and disable the button if lowest page is reached
      this.elements.paginations[id].btnPrev = btnGroup.createChildWithAttributes("button", "btn btn-outline-secondary" + ((pages.active == 0) ? " disabled" : ""), "«", {"aria-label": "Previous", "data-page": Math.max(pages.active - 1, 0)}).addEvent("click", (e) => {this._handlePageChange(e)});
      // loop trough all numbered page buttons and add them to the button group with their number as innerHTML. Also if the button is supposed to be the active one add the active class
      for (let i = 0; i < pages.pageNumbers.length; i++) {
        this.elements.paginations[id].btns[i] = btnGroup.createChildWithAttributes("button", "btn btn-outline-secondary" + ((pages.active == pages.pageNumbers[i]) ? " active" : ""), pages.pageNumbers[i] + 1, {"data-page": pages.pageNumbers[i]}).addEvent("click", (e) => {this._handlePageChange(e)});
      }
      // add the next button and disable the button if highest page is reached
      this.elements.paginations[id].btnNext = btnGroup.createChildWithAttributes("button", "btn btn-outline-secondary" + ((pages == pages.count) ? " disabled" : ""), "»", {"aria-label": "Next", "data-page": Math.min(pages.active + 1, pages.count - 1)}).addEvent("click", (e) => {this._handlePageChange(e)});
      // if there are more than three pages insert seperators after the first and before the last page button.
      if (pages.pageNumbers.length > 2) {
        // hide them if specified in pages object.
        this.elements.paginations[id].sepPrev = btnGroup.insertBefore(Util.createElementWithAttributes("button", "btn btn-outline-secondary ad-list-dots" + ((pages.separator[0] == false) ? " ad-list-dots-hidden" : ""), "···", {"aria-label": "separator", disabled: "true"}), btnGroup.childNodes[2]);
        this.elements.paginations[id].sepNext = btnGroup.insertBefore(Util.createElementWithAttributes("button", "btn btn-outline-secondary ad-list-dots" + ((pages.separator[1] == false) ? " ad-list-dots-hidden" : ""), "···", {"aria-label": "separator", disabled: "true"}), btnGroup.childNodes[pages.pageNumbers.length + 1]);
      }
    }

    _updatePagination(id) {
      // calculate the page numbers
      let pages = this._calcPageNumbers();
      // set the active page to the one returned in pages incase it is out ouf range.
      this.config.pageNumber = pages.active;
      // temporary variable for convenience
      let pagination = this.elements.paginations[id];
      // if the page count changed remove the pagination object and create it new
      if (pages.count != pagination.btnCount) {
        pagination.group.remove();
        this._addPaginationTo(id, pagination.container);
      } else {
        // loop trough all pages and
        for (let i = 0; i < pages.pageNumbers.length; i++) {
          // set the new page number text
          pagination.btns[i].innerText = pages.pageNumbers[i] + 1;
          // update the data-page attribute
          pagination.btns[i].setAttribute("data-page", pages.pageNumbers[i]);
          // toggle all buttons to inactive except the new active one
          pagination.btns[i].classList.toggle("active", (pages.active == pages.pageNumbers[i]));
        }
        // set a timeout to lose focus on all buttons
        setTimeout(() => {if ("activeElement" in document) {document.activeElement.blur()}}, 200);
        
      }
      // disable the previous button if the lowest page got reached
      pagination.btnPrev.classList.toggle("disabled", (pages.active == 0));
      // update the data-page attribute
      pagination.btnPrev.setAttribute("data-page", Math.max(pages.active - 1, 0));
      // set a timeout to lose focus
      setTimeout(() => pagination.btnPrev.blur(), 200);
      // disable the next button if the highest page got reached
      pagination.btnNext.classList.toggle("disabled", (pages.active == pages.count - 1));
      // update the data-page attribute
      pagination.btnNext.setAttribute("data-page", Math.min(pages.active + 1, pages.count - 1));
      // set a timeout to lose focus
      setTimeout(() => pagination.btnNext.blur(), 200);
      // update the visibility of the seperators
      if (this.elements.paginations[id].sepPrev) {
        this.elements.paginations[id].sepPrev.classList.toggle("ad-list-dots-hidden", pages.separator[0] == false);
        this.elements.paginations[id].sepNext.classList.toggle("ad-list-dots-hidden", pages.separator[1] == false);
      }
    }

    // calculate the page numbers and validate if the active one exsits
    _calcPageNumbers() {
      // calculate the total amount of pages
      let pageCount = Math.ceil(this.config.entitiesCount / this.config.entitiesPerPage);
      // ensure the current page number is not larger than the highest page
      let pageNumber = Math.min(pageCount, this.config.pageNumber);
      // if there is more than one page
      if (pageCount > 1) {
        // get the maximum number of pages that can be displayed on the pagination control
        let displayPageCount = Math.min(pageCount - 2, this.config.navMaxPageCount - 2)
        // get the minimum offset from the center of the displayable range to the begining of the complete range
        let minOffsetL = Math.floor((displayPageCount - 1) / 2)
        // calculate the offset 
        let offset = Math.min(pageCount - displayPageCount - 1, Math.max(pageNumber - minOffsetL, 1));
        // generate a array with all the page numbers
        let pageNumbers = [
          ...[0],
          ...[...Array(displayPageCount).keys()].map(x => x + offset),
          ...[pageCount - 1]];
        // return the page numbers, count and active page
        // also return whethach separator is visible
        return {active: pageNumber, count: pageCount, pageNumbers: pageNumbers, separator: [(offset > 1) ? true : false, (offset < pageCount - 1 - displayPageCount) ? true : false]};
      }
      // if there are less than two pages return standard values
      return {active: 0, count: 1, pageNumbers: [0]};
    }

    // draw skeleton structure in case the loading takes a long time
    _drawSkeleton() {
      // if the skeleton function is vald
      if (this.config.skeletonFunction && typeof this.config.skeletonFunction == "function") {
        // get a templat for a skeleton entity
        let template = this.config.skeletonFunction();
        // put it inside the list multiple times
        for (let i = 0; i < this.config.entitiesPerPage; i++) {
          this.elements.skeletonBody.appendChild(template.cloneNode(true));
        }
      }
    }

    // toggle the advanced menu visible
    _toggleAdvanced(to) {
      // if 'to' was passed, set menu to visible / hidden
      // if not just toggle the class
      if (to == undefined) {
        this.elements.advanced.container.classList.toggle("ad-list-advanced-visible");
      } else {
        this.elements.advanced.container.classList.toggle("ad-list-advanced-visible", to);
      }
    }

    // explicitly initiating a search
    _handleSearch(e) {
      // if there is a timout set clear it
      // there should not be another event from any input changes after explicitly initiating a search
      if (this.timeout != null) {
        clearTimeout(this.timeout);
      }
      // dispatch change event
      this._controlsChange();
      // load new entities
      this.load();
    }

    // due to modifications to input elements which are 'live' or 'onchange' a reload is needed.
    _handleInputChange(e) {
      // if there is already a timeout set clear it
      if (this.timeout != null) {
        clearTimeout(this.timeout);
      }
      // set new timeout
      this.timeout = setTimeout(() => {
        // set timeout bac to null
        this.timeout = null;
        // dispatch change event
        this._controlsChange();
        // load new entities
        this.load();
        // use timout duration from config
      }, this.config.timeout);
    }

    // handle page change (arrow function because of 'this' scope)
    _handlePageChange(e) {
      // get the new page index
      let newActive = e.target.getAttribute("data-page");
      // change to the new page
      this.ChangePageTo(newActive);
      // dispatch change event
      this._controlsChange();
      // load the new entities
      this.load();
    }

    _controlsChange() {
      let event = new CustomEvent("ad-control-change", {detail: {obj: this, vals: this.getFormVals()}});
      // Dispatch the event.
      this.elements.root.dispatchEvent(event);
    }

    // change to the page number passed
    ChangePageTo(newActive) {
      // set the new page number
      this.config.pageNumber = newActive
      // update all pagination elements
      for (let pagination in this.elements.paginations) {
        if (!this.elements.paginations.hasOwnProperty(pagination)) continue;
        this._updatePagination(pagination)
      }
    }

    load() {
      let retVals = {form: this.getFormVals()};
      retVals.listPageEntitiesCount = this.config.entitiesPerPage;
      retVals.listPageNumber = this.config.pageNumber;
      retVals.list = this;
      this.handler(retVals);
      // update all pagination elements
      /*for (let pagination in this.elements.paginations) {
        if (!this.elements.paginations.hasOwnProperty(pagination)) continue;
        this._updatePagination(pagination)
      }*/ 
    }

    getFormVals() {
      // value to collect all input values
      let vals = {};
      // loop trough all forms
      this.elements.forms.forEach(form => {
        // process each form and add values to vals
        form.process(vals);
      });
      vals.pageNumber = this.config.pageNumber;
      return vals;
    }

    setFormVals(vals) {
      if (vals) {
        for (let input in this.elements.inputs) {
          if (this.elements.inputs.hasOwnProperty(input)) {
            let inputElem = this.elements.inputs[input];
            if (vals[inputElem.name]) {
              inputElem.value = vals[inputElem.name];
            }
          } 
        }
        if (vals.pageNumber != undefined) {this.ChangePageTo(vals.pageNumber)}
      }
    }

    removeEntities() {
      this.elements.body.removeChildren();
    }

    addEntity(entity) {
      this.elements.body.createChild("div", "ad-list-body-entity").appendChild(entity);
    }
  }
  return List
})();

export default List