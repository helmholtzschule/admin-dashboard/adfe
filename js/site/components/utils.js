var _app = null;

const Util = (() => {
  // collection of usefull functions
  const Util = {
    // sends a request and calls the function provided in callback
    RequestAsync(method, url, callback, data = null, headers = null) {
      // check whether callback is a function
      if (typeof callback != "function") {
        return false
      }
      // if headers are provided they have to be a object in the form [{key:"key", value:"value"}]
      if ((typeof headers != "object") && (headers != null)) {
        return false
      }
      // if data is provided it has to be a object
   /*   if ((typeof data != "object") && (data != null)) {
        return false
      }*/
      // create new request object
      var req = new XMLHttpRequest();
      // open request
      req.open(method, url);
      // if headers are provided
      if (headers != null) {
        headers.forEach(header => {
          // check whether key and value are both defined
          if (header.key == undefined || header.value == undefined) {
            console.warn(`skiped header "${header.key}: ${header.value}" as key or value where undefined`);
            return
          }
          // set header on the request
          req.setRequestHeader(header.key, header.value);
        });
      }
      // allow to send credentials
      req.withCredentials = true;
      // once the response is loaded completly
      req.onload = function () {
        // if the response body is not empty
        if (req.responseText != "") {
          // parse the json body to an javascript object
          // call the callback with the recieved data
          let retObj = null
          try {
            retObj = JSON.parse(req.responseText);
          } catch (e) {
            retObj = {};
          }
          callback(retObj);
        } else {
          // call the callback without any data
          callback({});
        }
      }
      // send the request
      req.send(JSON.stringify(data));
      console.log(JSON.stringify(data))
      // return the request
      return req
    },
    // load a resource and replace a skeleton element
    loadResource(adid, object, replace, draw, force=false) {
      // if entity was already loaded and loading is not forced use the cached one
      if (_app.Entities[adid] && !force) {
        // replace the old element with the new drawn
        replace.parentNode.replaceChild(_app.Entities[adid][draw](), replace);
      } else {
        // send request to get entity by id
        Util.RequestAsync("GET", _app.adapiUrl("/api/id/" + adid), (data) => {
          // if there where any errors show them in the notification area
          if (data.Errors) {
            data.Errors.forEach(error => {
              _app.notification.createNewError(error);
            });
          } else {
            // create a new object and replace the old element with the new drawn
            let obj = data[0];
            _app.Entities[obj._adid] =  new object(obj);
            replace.parentNode.replaceChild(_app.Entities[obj._adid][draw](), replace);
          }
        });
      }
    },

    toNumIfPossible(val) {
      return (Number(val)) ? Number(val) : val
    },

    // create element by tag name, class name (optional) and inner text (optional)
    createElement(type="div", className="", text) {
      // create the element with specific type
      let elem = document.createElement(type);
      // set the class name
      elem.className = className;
      // if text was provided set the innerHTML to text
      if (text) {
        elem.innerHTML = text;
      }
      // return the element for 'chaining'
      return elem;
    },
    // similar to createElement() but set attributes passed in attributes object
    createElementWithAttributes(type="div", className="", text, attributes={}) {
      // create element with createElement()
      let elem = this.createElement(type, className, text);
      // for every attribute passed
      for (let attribute in attributes) {
        // if the attribute isn't a property of the html object proced with the next attribute
        if (!attributes.hasOwnProperty(attribute)) continue;
        // set the attribute
        elem.setAttribute(attribute, attributes[attribute]);
      }
      // return the element for 'chaining'
      return elem;
    },
    // set the property of a object defined by prop (array)
    setToValue(obj, value, prop) {
      let i;
      for (i = 0; i < prop.length - 1; i++) {
        obj = obj[prop[i]];
      }
      obj[prop[i]] = value;
    }
  }
  // method on html elements for convenience
  // similar to createElement() but appends the object as a child
  HTMLElement.prototype.createChild = function (type="div", className="", text) {
    let elem = Util.createElement(type, className, text);
    this.appendChild(elem);
    // return the element for 'chaining'
    return elem;
  }
  // similar to createElementWithAttributes() but appends the object as a child
  HTMLElement.prototype.createChildWithAttributes = function (type="div", className="", text, attributes={}) {
    let elem = Util.createElementWithAttributes(type, className, text, attributes);
    this.appendChild(elem);
    // return the element for 'chaining'
    return elem;
  }
  // similar to addEventHandler but returns the element for 'chaining'
  HTMLElement.prototype.addEvent = function (type, listener, bubble) {
    this.addEventListener(type, listener, bubble);
    // return the element for 'chaining'
    return this;
  }
  // remove all children of the node
  HTMLElement.prototype.removeChildren = function () {
    // while there are children
    while (this.lastChild != null) {
      // delete them
      this.lastChild.remove();
    }
  }
  // shorthandfunction for adding a backreference to a element
  HTMLElement.prototype.referenceTo = function (key, to) {
    this[key] = to;
    // return new element for further use
    return this;
  }
  // bind value of input to object
  HTMLElement.prototype.bind = function (obj, prop, events=["change"]) {
    events.forEach(event => {
      this.addEventListener(event, (e) => {
        if (this.value != undefined && prop != undefined) {
          Util.setToValue(obj, this.value, prop);
        }
        console.log(this, obj);
      });
    });
    return this;
  }

  // method on html form elements for convenience
  // get the values of user inputs in a form as object in the form {name: value}
  HTMLFormElement.prototype.process = function(values=null) {
    // all element types that can get submitted
    const SUBMITTABLE = /^(?:input|textarea|select)$/i;
    // all element types that can initiate a submition
    const SUBMITTER_TYPES = /^(?:button|submit|reset|image|file)$/i;
    const CHECKBOX = /^checkbox$/i;
    const RADIO = /^radio$/i;
    // use parameter if values was passed 
    // if not use a empty object
    values = (values) ? values : {}
    // loop trough all objects listed in the form as inputs
    for (let i = 0; i < this.elements.length; i++) {
      // temporary variable for convenience
      let elem = this.elements[i];
      // check if the element is valid
      if (elem.name && !elem.disabled && SUBMITTABLE.test(elem.nodeName) && !SUBMITTER_TYPES.test(elem.type)) {
        if (CHECKBOX.test(elem.type)) {
          // if it is a checkbox set the value to true/ false
          values[elem.name] = elem.checked;
        } else if (RADIO.test(elem.type)) {
          // if it is a radio element and checked set the value of the radio as value for the name
          // if no radio element of the same name is checked the value of the name will be undefined
          values[elem.name] = (elem.checked) ? elem.value : values[elem.name];
        } else {
          // if it is a normal element set the value for the name
          values[elem.name] = elem.value;
          continue;
        }
      }
    }
    // return values
    return values;
  }

  HTMLFormElement.prototype.customValidate = function(callback, callbackWhenInvalid=false) {
    this.addEventListener('submit', function(event) {
      this.classList.add('was-validated');
      if (this.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        if (callbackWhenInvalid && callback != undefined) {
          callback(event);
        }
      }
      console.log(callback)
      if (callback != null) {
        console.log(event)
        callback(event);
      }
    }, false);
  }

  return Util
})()


const Link = (() => {
  const Link = {
    window(entity, text, page="dashboard") {
      let linkElm = Util.createElement("a", "ad-link-entity", text);
      linkElm.href = _app.adUrl(page, {app: {view: "single", id: ((typeof entity == "object") ? entity._adid : entity)}});
      return linkElm
    },

    modal(entity, text, eClass, mObject, config) {
      let linkElm = Util.createElement("a", "ad-link-entity", text);
      if (typeof entity == "object") {
        linkElm.addEvent("click", () => {mObject.create(entity.drawModal(), config)});
      } else {
        Util.RequestAsync("GET", _app.adapiUrl("/api/id/" + entity), (data) => {
          entity = new eClass(data[0]);
          linkElm.addEvent("click", () => {mObject.create(entity.drawModal(), config)});
        });
      }
      return linkElm
    }
  }
  return Link
})()


export function initUtil(app) {
  _app = app;
}
export default Util
export {Link}