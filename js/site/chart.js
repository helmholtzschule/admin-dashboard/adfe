// do not use for now

const SVG_NS = "http://www.w3.org/2000/svg";

class Chart extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.axis = null;
    this.plots = null;
    this.svg = document.createElementNS(SVG_NS, "svg");
    this.appendChild(this.svg);
    this.details = this.createElementComplete("div", "ad-chart-details");
    this.margin = [8, 8, 8, 8];
    this.bgLineMinDistance = 0;
    this.maxBGLines = 8;
  }

  get width() {
    return this.offsetWidth;
  }

  get height() {
    return this.offsetHeight;
  }
/*
  get maxWidth() {
    let v = 0;
    this.plots.forEach(plot => {
      v = Math.max(v, ...plot.dataX)
    });
    return v;
  }

  get minWidth() {
    let v = Number.MAX_SAFE_INTEGER;
    this.plots.forEach(plot => {
      v = Math.min(v, ...plot.dataX)
    });
    return v;
  }

  get maxHeight() {
    let v = 0;
    this.plots.forEach(plot => {
      v = Math.max(v, ...plot.dataY)
    });
    return v;
  }

  get minHeight() {
    let v = Number.MAX_SAFE_INTEGER;
    this.plots.forEach(plot => {
      v = Math.min(v, ...plot.dataY)
    });
    return v;
  }
*/
  setData(data) {
    this.axiss = data.axiss;
    this.plots = data.plots;
  }

  calcMargin() {
    if (this.axiss != null) {
      let cm = [0, 0, 0, 0];
      this.axiss.forEach(axis => {
        switch (axis.dir) {
          case "xp":
            cm[3] += axis.space
            break;
          case "xs":
            cm[1] += axis.space;
            break;
          case "yp":
            cm[2] += axis.space;
            break;
          case "ys":
            cm[0] += axis.space;
            break;
        }
      });
      for (let i = 0; i < cm.length; i++) {
        cm[i] = Math.max(cm[i], this.margin[i]);
      }
      return cm;
    } else {
      return this.margin;
    }
  }

  mapOnAxis(axis, margin, value) {
    if (axis.dir.startsWith("x")) {
      // standard map but take margin into account
      return (value - axis.from) * ((this.width - margin[1]) - margin[3]) / (axis.to - axis.from) + margin[3];
    } else {
      // standard map but take margin into account
      return (value - axis.from) * ((this.height - margin[2]) - margin[0]) / (axis.to - axis.from) + margin[0];
    }
  }

  drawChartBackground() {
    this.svg.bg = this.svg.createElementComplete("g");
    this.svg.bg.setAttribute("class", "ad-chart-bg");
    this.axiss.forEach(axis => {
      let m = this.calcMargin();
      let interval = getInterval(axis.to - axis.from, this.maxBGLines);
      if (axis.dir.startsWith("x")) {
        // draw vertical lines for horizontal axiss
        let lLength = this.height - m[0] - m[2];
        let verticalGroup = this.svg.bg.createElementComplete("g");
        verticalGroup.setAttribute("class", "ad-chart-bg-vertical");
        for (let i = axis.from; i < axis.to; i += interval) {
          let lineGroup = verticalGroup.createElementComplete("g");
          lineGroup.drawLine(lLength, false);
          lineGroup.drawText(i + axis.unit, false)
          lineGroup.setAttribute("transform", `translate(${this.mapOnAxis(axis, m, i)}, ${m[0]})`)
          // draw text (shift if inbetween)
        }
        let lineGroup = verticalGroup.createElementComplete("g");
        lineGroup.drawLine(lLength, false);
        lineGroup.setAttribute("transform", `translate(${this.mapOnAxis(axis, m, axis.to)}, ${m[0]})`)
        // draw text if not shift inbetween
      } else {
        // draw horizontal lines for vertical axiss
        let lLength = this.width - m[1] - m[3];
        let horizontalGroup = this.svg.bg.createElementComplete("g");
        horizontalGroup.setAttribute("class", "ad-chart-bg-horizontal");
        for (let i = axis.to; i > axis.from; i -= interval) {
          let lineGroup = horizontalGroup.createElementComplete("g");
          lineGroup.drawLine(lLength, true);
          lineGroup.drawText(i + axis.unit, true)
          lineGroup.setAttribute("transform", `translate(${m[3]}, ${this.mapOnAxis(axis, m, i)})`)
          // draw text (shift if inbetween)
        }
        let lineGroup = horizontalGroup.createElementComplete("g");
        lineGroup.drawLine(lLength, true);
        lineGroup.setAttribute("transform", `translate(${m[3]}, ${this.mapOnAxis(axis, m, axis.from)})`)
        // draw text if not shift inbetween
      }
    });
  }

  render() {
    console.log("render");
  }

  update() {
    console.log("update");
  }
}

class ChartBar extends Chart {
  render() {
    console.log("render2");
  }

  update() {
    console.log("update2");
  }
}


// register the custom element
customElements.define("ad-chart", Chart);
customElements.define("ad-bar-chart", ChartBar);

exmp = {
  axiss: [
    {
      dir: "xp",
      label: "this",
      label_inbetween: true,
      unit: "",
      // numbers as category is ""
      from: 0,
      // if zero calculate automatic
      to: 100,
      // if zero calculate automatic
      unit_array: null,
      // if a category should be used as unit unit_array has to be an array
      space: 80
    },
    {
      dir: "yp",
      label: "that",
      label_inbetween: false,
      unit: "m",
      from: -1000,
      to: 4000,
      unit_array: null,
      space: 64
    }
  ],
  plots: [{
    name: "something",
    x: 0,
    y: 1,
    dataX: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    dataY: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
  }]
}

// copy from notification.js
// shorthand method for creating a html element from name classes and inner html
HTMLElement.prototype.createElementComplete = function (type, className="", innerHTML="") {
  let elem = document.createElement(type);
  elem.className = className;
  elem.innerHTML = innerHTML;
  this.appendChild(elem);
  // return new element for further use
  return elem;
}

// shorthandfunction for adding an eventHandler to a element
HTMLElement.prototype.addEventHandler = function (type, handler, options=false) {
  this.addEventListener(type, handler, options);
  // return new element for further use
  return this;
}

SVGElement.prototype.createElementComplete = function (type, className="") {
  let elem = document.createElementNS(SVG_NS, type);
  elem.className = className;
  this.appendChild(elem);
  // return new element for further use
  return elem;
}

SVGElement.prototype.drawLine = function (length, horizontal) {
  let line = this.createElementComplete("line");
  line.setAttribute("stroke", "#000");
  if (horizontal) {
    line.setAttribute("x2", length);
  } else {
    line.setAttribute("y2", length);
  }
}

SVGElement.prototype.drawText = function (text, horizontal) {
  let textE = this.createElementComplete("text");
  textE.innerHTML = text
  if (horizontal) {
    textE.setAttribute("dy", "0.2em");
    textE.setAttribute("x", - textE.getBBox().width - 8);
  } else {
    textE.setAttribute("dy", "1em");
    textE.setAttribute("y", textE.getBBox().height);
    textE.setAttribute("x", - textE.getBBox().width / 2);
  }
}

function getInterval(n, maxInterval) {
  let int = n / maxInterval;
  let len = Math.log(int) * Math.LOG10E + 1 | 0;
  let newInt = 0;
  for (i of [1, 2, 2.5, 5]) {
    newInt = i * Math.pow(10, len - 1);
    if (newInt >= int) {
      break
    }
  }
  return newInt;
}