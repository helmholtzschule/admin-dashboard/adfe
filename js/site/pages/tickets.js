import authenticate from "../components/auth.js"
import Util, {Link, initUtil} from "../components/utils.js"
import List from "../components/list.js"
import Ticket, {TicketSkeleton, initTicket} from "../components/ticket.js"
import App from "../components/app.js"

if (authenticate()) {
  let app = new App({}, (app) => {
    initTicket(app);

    let list = new List(app.elements.contentWrapper, {skeletonFunction: TicketSkeleton.drawList, loadOnConstruct: false, inlineControls:[{type:"checkbox", name: "ticket-archived", label: "show archived", onchange: true}]}, handleListChange);
    app.addUserControl(list);
    Util.RequestAsync("GET", app.adapiUrl("/api/tickets/stats"), (data) => {
      list.entitiesCount = data.count;
      app.loadControlValues("list");
      list.load();
    });
  });

  function handleListChange(vals) {
    let find = {};
    if (!vals.form["ticket-archived"]) {
      find["ticket.archived"] = false;
    }
    let search = vals.form["search"];
    if (search != "") {
      find["$or"] = [{"ticket.title":{$regex:`.*${search}.*`, $options: "i"}}, {"ticket.description":{$regex:`.*${search}.*`, $options: "i"}}]
    }
    let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount, find: find}
    Util.RequestAsync("POST", app.adapiUrl("/api/tickets"), (data) => {
      if (data != null) {
        if (data.Errors) {
          data.Errors.forEach(error => {
            app.notification.createNewError(error);
          });
        } else {
          vals.list.removeEntities();
          data.forEach(ticket => {
            app.Entities[ticket._adid] =  new Ticket(ticket);
            vals.list.addEntity(app.Entities[ticket._adid].drawList());
          });
        }
      } else {
        vals.list.removeEntities();
      }
    }, dat);
  }
}