import authenticate from "../components/auth.js"
import Util, {Link, initUtil} from "../components/utils.js"
import List from "../components/list.js"
import User, {UserSkeleton} from "../components/user.js"
import App from "../components/app.js"

if (authenticate()) {
  let app = new App({}, (app) => {
    
    let list = new List(app.elements.contentWrapper, {skeletonFunction: UserSkeleton.drawList, loadOnConstruct: false}, handleListChange);
    app.addUserControl(list);
    Util.RequestAsync("GET", app.adapiUrl("/api/users/stats"), (data) => {
      list.entitiesCount = data.count;
      app.loadControlValues("list");
      list.load();
    });
    app.user = {"access-level": 9}
    if (app.user["access-level"] >= 7) {
      let container = Util.createElement("div", "ad-users-new-container");
      app.elements.contentWrapper.createChild("button", "btn btn-block btn-outline-secondary", "+").addEvent("click", () => {
        container.removeChildren();
        let user = container.createChild("form", "ad-users-new");
        user.customValidate(((e) => {
          console.log("process");
          e.preventDefault();
          let d = user.process();
          if (d["password"] == d["confirm"]) {
            console.log("same")
            let emails = [];
            if (d["public-email1"] != "") {emails.push(d["public-email1"])}
            if (d["public-email2"] != "") {emails.push(d["public-email2"])}
            Util.RequestAsync("POST", app.adapiUrl("/user/create"), (ret) => {
              console.log(ret)
            }, {
              username: d["username"],
              password: d["password"],
              name: d["name"],
              "access-level": d["access-level"],
              "private-email": d["private-email"],
              "public-emails": emails,
              "icon-link": d["icon-link"],
            });
          }
        }));
        let newUser = new User({});
        user.createChildWithAttributes("input", "form-control", null, {name: "name", placeholder: "name", required: "", type: "text"});
        user.createChildWithAttributes("input", "form-control", null, {name: "username", placeholder: "username", required: "", autocomplete: "off", type: "text"});
        user.createChildWithAttributes("input", "form-control", null, {name: "password", placeholder: "password", required: "", autocomplete: "off", type: "password"});
        user.createChildWithAttributes("input", "form-control", null, {name: "confirm", placeholder: "confirm password", required: "", autocomplete: "off", type: "password"});
        user.createChildWithAttributes("input", "form-control", null, {name: "access-level", placeholder: "access level", required: "", type: "number", value: app.user["access-level"], min: 0, max: app.user["access-level"]});
        user.createChildWithAttributes("input", "form-control", null, {name: "private-email", placeholder: "private email", required: "", type: "email"});
        user.createChildWithAttributes("input", "form-control", null, {name: "public-email1", placeholder: "first public email", type: "text"});
        user.createChildWithAttributes("input", "form-control", null, {name: "public-email2", placeholder: "second public email", type: "text"});
        user.createChildWithAttributes("input", "form-control", null, {name: "icon-link", placeholder: "icon link", type: "text"});
        let preview = user.createChild("div", "ad-users-new-preview");
        user.createChildWithAttributes("input", "btn btn-success btn-block", null, {name: "submit", value: "add user", type: "submit"});
        user.addEvent("change", change).addEvent("keyup", change);
        function change() {
          compData();
          preview.removeChildren();
          preview.appendChild(newUser.drawList());
        }
        function compData() {
          let d = user.process();
          newUser.data = {
            ...{_adid: "US42"},
            ...{username: d["username"], name: d["name"], "public-emails": [d["public-email1"], d["public-email2"]], "icon-link": d["icon-link"], "access-level": d["access-level"]}
          }
        }
      });
      app.elements.contentWrapper.appendChild(container);
    }
  });

  function handleListChange(vals) {
    let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount}
    Util.RequestAsync("POST", app.adapiUrl("/api/users"), (data) => {
      if (data.Errors) {
        data.Errors.forEach(error => {
          app.notification.createNewError(error);
        });
      } else {
        vals.list.removeEntities();
        data.forEach(user => {
          app.Entities[user._adid] =  new User(user);
          vals.list.addEntity(app.Entities[user._adid].drawList());
        });
      }
    }, dat);
  }
}