import Util, {Link} from "./components/utils.js"
import List from "./components/list.js"
import Ticket, {TicketSkeleton} from "./components/ticket.js"
import Notification from "./components/notifications.js"
import Modals from "./components/modal.js"


class Reference {
  constructor(str, textIndex) {
    let re = new RegExp("[A-Z]+");
    let match = re.exec(str);
    this.type = match[0];
    this.number = parseInt(str.substring(match[0].length + 1));
    this.textIndex = textIndex;
  }

  toString() {
    return "#" + this.type + this.number;
  }
}

function getReferences(str) {
  let re = new RegExp("#[A-Z]+[0-9]+", "g");
  let res, resArr = []; 
  while(res = re.exec(str)) {
    console.log(res);
    resArr.push({match: res[0], index: res.index});
  }
  return resArr;
}

function linkToID(content, id, popup=true) {
  let fn = "openIdInWindow"
  if (popup) {
    fn = "openIdInPopup"
  }
  return `<a onclick=${fn}('${id}')>${content}</a>`
}

let notification = new Notification();

let Tickets = {};

//Util.RequestAsync("GET", "http://localhost:8080/api/tickets", TicketHandler)

function handleListChange(vals) {
  let dat = {from: vals.listPageNumber * vals.listPageEntitiesCount, count: vals.listPageEntitiesCount}

  Util.RequestAsync("POST", "http://localhost:8080/api/tickets", (data) => {
    if (data.Errors) {
      data.Errors.forEach(error => {
        notification.createNewError(error);
      });
    } else {
      vals.list.removeEntities();
      data.forEach(ticket => {
        Tickets[ticket._adid] =  new Ticket(ticket);
        vals.list.addEntity(Tickets[ticket._adid].drawList());
      });
    }

  }, dat);
}

let tcont = document.getElementById("test-container");
let list = new List(tcont, {skeletonFunction: TicketSkeleton.drawList, loadOnConstruct: false}, handleListChange);

Util.RequestAsync("GET", "http://localhost:8080/api/tickets/stats", (data) => {
  list.entitiesCount = data.count;
  list.load();
});